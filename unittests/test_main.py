import sys
import mock
# import pytest

import main


def test_run():
    test_string = "====3"
    mock_platform = sys.platform
    assert main.run(test_string) == test_string + mock_platform


def test_init():
    with mock.patch.object(main, "run", return_value=42):
        with mock.patch.object(main, "__name__", "__main__"):
            with mock.patch.object(main.sys, 'exit') as mock_exit:
                main.init()
                assert mock_exit.call_args[0][0] == 42
